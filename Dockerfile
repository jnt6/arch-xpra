FROM base/archlinux

RUN pacman -Syu --noconfirm
RUN ln -s core_perl/pod2man /usr/bin/pod2man
RUN pacman -S --noconfirm openssh firefox gnome-terminal git man make base-devel binutils perl zsh vim gst-python2 xterm tmux vi xorg xpra rsync
COPY sudoers /etc/sudoers
RUN chown root:root /etc/sudoers
RUN chmod 400 /etc/sudoers
#RUN useradd --no-create-home --shell=/bin/false build && usermod -L build
#RUN cd /tmp/ && git clone https://aur.archlinux.org/cower.git && cd /tmp/cower && source /tmp/cower/PKGBUILD && pacman -S --noconfirm --needed --asdeps "${makedepends[@]}" "${depends[@]}"
#RUN chown -R build /tmp/cower
#USER build
#RUN cd /tmp/cower && makepkg --skippgpcheck
#USER root
#RUN pacman -U --noconfirm /tmp/cower/*.pkg.tar.xz
#RUN cd /tmp/ && git clone https://aur.archlinux.org/pacaur.git && cd /tmp/pacaur && source /tmp/pacaur/PKGBUILD && pacman -S --ignore cower --noconfirm --needed --asdeps "${makedepends[@]}" $(echo "${depends[@]}" | perl -pe 's/cower//g')
#RUN chown -R build /tmp/pacaur
#USER build
#RUN cd /tmp/pacaur && makepkg --skippgpcheck
#USER root
#RUN pacman -U --noconfirm /tmp/pacaur/*.pkg.tar.xz
RUN curl -L https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz > /tmp/okd.tar.gz
RUN mkdir /tmp/okd
RUN tar -xzf /tmp/okd.tar.gz -C /tmp/okd
RUN mv /tmp/okd/*/oc /bin/oc
RUN mv /tmp/okd/*/kubectl /bin/kubectl
RUN echo 'LANG=en_US.UTF-8' > /etc/locale.conf
RUN echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
RUN locale-gen
RUN useradd -m user -g root && usermod -L user
RUN mkdir /nix
RUN chmod g+w /nix
USER user
RUN curl https://nixos.org/nix/install > /tmp/nixos
RUN USER=user sh /tmp/nixos --no-daemon
RUN mkdir ~/.ssh
RUN chmod 700 ~/.ssh
RUN chmod -R g+rwx /home/user
USER root
# The nix stuff may be volume mounted over
RUN chmod g+w /
RUN mv /nix /nix_orig
RUN mkdir /nix
RUN chmod g+w /nix
COPY entrypoint.sh /entrypoint.sh
COPY sshd_config /user_sshd_config
COPY 55_server_x11.conf /etc/xpra/conf.d/55_server_x11.conf
RUN chmod g+w /run/user
RUN chown root:root /entrypoint.sh
RUN chmod +x /entrypoint.sh
RUN chmod g+w /etc/passwd
user user
RUN cd /home/user
EXPOSE 2222
CMD /entrypoint.sh
