This is just a little toy you can run in stevedore (or wherever) so you can run GUI stuff via xpra.

# NIX package manager

I thought it might be fun to be able to install software in these containers. Since this is arch linux you can use 'pacman', but that of course requires root, which you may not have.

I've pre-installed nix and configured it to be usable by the user running the entrypoint. For this to actually work, you need to mount /nix in as a volume, then you can install/run stuff with `nix-env --install <whatever>`

# Running in Openshift

Kubernetes won't let you run stuff as root, so a lot of the hackiness here is to work around that. Pay attention to the following workarounds:

1. During the build, /etc/passwd is made group writable. The openshift user is given an arbitrary high UID but always GID 0, so this allows it to write out directly to /etc/passwd
1. The entrypoint.sh command 'fixes' /etc/password to give the user a valid passwd entry - required (or at least very helpful) for a variety of reasons
1. entrypoint.sh also makes the run directory; usually done via pam (which we can't use, since we aren't root...)
1. A persistent volume is expected to be present at /home/user, where keys will live. authorized_keys comes from an environment variable you can set on the deployment
1. The ssh daemon is run as the normal user, on port 2222 - note it uses the ssh config in this repo not the normal system one

The openshift yaml files can make your own service in openshift using `oc apply -f <file>`. Pay special attention to the `service.yaml` file which sets up a nodeport - this is the port that you will actually ssh to on the external nodes
