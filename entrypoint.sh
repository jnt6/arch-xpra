#!/bin/bash
# Run as the normal user in openshift
perl -pe "s/:1001:/:$(id -u):/" /etc/passwd > /tmp/passwd
cat /tmp/passwd > /etc/passwd
chown user /home/user
export HOME=/home/user
export USER=user
# If the nix directory is empty, populate it with the default from the build
# This assumes a volume mount present
if [ ! -d /nix/var ]; then
    echo "Going to copy over a base nix install"
    rsync -a /nix_orig/ /nix

fi
# Also set up the user's environment like the nix installer would
if ! grep -q nix /home/user/.bash_profile; then
    ln -s /nix/var/nix/profiles/per-user/user/profile /home/user/.nix-profile
    echo 'if [ -e /home/user/.nix-profile/etc/profile.d/nix.sh ]; then . /home/user/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer' >> /home/user/.bash_profile
fi
# xpra needs a place to put its socket
mkdir -p /run/user/$(id -u)
# You probably want to mount this homedir as a volume really
mkdir -p ~/.ssh/etc/ssh
chmod 700 ~/.ssh
cp /user_sshd_config ~/.ssh/sshd_config
echo "${AUTHORIZED_KEYS}" > ~/.ssh/authorized_keys
ssh-keygen -A -f ~/.ssh

/usr/bin/sshd -e -f ~/.ssh/sshd_config -D -p 2222
